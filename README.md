The Irish Jewelry Company celebrates their Celtic heritage and a love of Ireland through original Irish Jewelry design. Their beautiful Irish jewelry is steeped in Celtic symbolism and rich in Irish tradition.

Website: https://www.theirishjewelrycompany.com
